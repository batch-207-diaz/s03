-- crudnotes.sql

INSERT INTO artists (name) VALUES ("Incubus");
INSERT INTO artists (name) VALUES ("Psy");

SELECT * FROM artists;

INSERT INTO artists (name) VALUES ("Ichika");
INSERT INTO artists (name) VALUES ("Covet");
INSERT INTO artists (name) VALUES ("Adrian Talens");

SELECT * FROM artists;

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Make Yourself", "1999-10-26",1);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Psy 6", "2012-1-15",1);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("she waits patiently", "2018-04-01",3);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Currents", "2015-12-20",4);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Poly Bridge OST", "2016-03-28",5);

SELECT * FROM albums;

INSERT INTO songs (song_name,length,genre,album_id) 
VALUES ("Gangnam Style",253,"K-pop",2),
("Pardon Me",223,"Rock",1),
("Drive",153,"Rock",1);

INSERT INTO songs (song_name,length,genre,album_id) 
VALUES ("illusory sense",348,"Progressive Rock",3),
("Nautilus",355,"Math Rock",4),
("Along for the Ride",255,"Game Soundtrack",5);

illusory sense Progressive Rock 348
Nautilus Math Rock 355
Along for the Ride Soundtrack 255

SELECT song_name FROM songs;
SELECT album_title, artist_id FROM albums;
SELECT song_name FROM songs WHERE genre = "Math Rock"; -- filter
SELECT song_name, genre FROM songs WHERE length > 200; -- logical operator
SELECT song_name FROM songs WHERE genre = "game soundtrack"; -- not case sensitive
SELECT song_name FROM songs WHERE genre = "Rock" AND length < 200; -- and 
SELECT song_name FROM songs WHERE genre = "Rock" OR length > 200; -- or 

INSERT INTO songs (song_name, length, genre, album_id)
VALUES ("Megalomaniac",410,"Classical",2);

UPDATE songs SET song_name = "Stellar", genre = "Rock", album_id = 1 WHERE song_name = "Megalomaniac";
UPDATE songs SET length = 200 WHERE song_name = "Stellar";


/* 

=====================
SQL CRUD Operations
=====================

1. (Create) Adding a record.
    Terminal
        Adding a record:
        Syntax
            INSERT INTO table_name (column_name) VALUES (value1);

2. (Read)Show all records.
    Terminal

        Displaying/retrieving records:
        Syntax
            SELECT column_name FROM table_name;

3. (Create) Adding a record with multiple columns.
    Terminal

        Adding a record with multiple columns:
        Syntax
            - INSERT INTO table_name (column_name, column_name) VALUES (value1, value2);

            --Note: Values must match the column.

5. (Create) Adding multiple records.
    Terminal

        Adding multiple records:
        Syntax
            INSERT INTO table_name (column_name, column_name) VALUES (value1, value2), (value3, value4);


6. Show records with selected columns.
    Terminal

       Retrieving records with selected columns:
        Syntax
            SELECT (column_name1, column_name2) FROM table_name;

        SELECT song_name, genre FROM songs;

7. (Read) Show records that meet a certain condition.
    Terminal

        Retrieving records with certain condtions:
        Syntax
            SELECT column_name FROM table_name WHERE condition;


8. (Read) Show records with multiple conditions.
    Terminal

        Displaying/retrieving records with multiple conditions:
        Syntax
            AND CLAUSE
                SELECT column_name FROM table_name WHERE condition1 AND condition2;
            OR CLAUSE
                SELECT column_name FROM table_name WHERE condition1 OR condition2;

9. (Update) Updating records.
    Terminal

        Add a record to update:
        INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Megalomaniac", 410, "Classical", 2);

        Updating records:
        Syntax
            UPDATE table_name SET column_name = value WHERE condition;

            UPDATE table_name SET column_name = value, column_name2 = value2 WHERE condition;

            --Note: When updating and deleting, add a where clause or else you may update or delete all items in a table.

10. (Delete) Deleting records.
    Terminal
        Deleting records:
        Syntax
            DELETE FROM table_name WHERE condition;

*/